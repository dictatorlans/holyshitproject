<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\View\View;
use App\Models\Course;

class CoursesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index()
    {
        return view('courses.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Выводит страницу с добавлением курса
     *
     * @return View
     */
    public function getAdd()
    {
        $categories = Category::all();
        return view('courses.add')->with('categories', $categories);
    }

    /**
     * Добавление курса в БД
     *
     * @return View
     */
    public function postAdd(Request $request)
    {
        $categories = Category::all();

        $courseName = $request->input('name');
        $courseDescription = $request->input('descr');
        $coursePrice = $request->input('price');
        $courseCategory = $request->input('category');
        if($coursePrice == 0)$courseType = 'free';
        else $courseType = 'paid';

        $course = new Course(array(
            'name'          => $courseName,
            'description'   => $courseDescription,
            'cost'          => $coursePrice,
            'type'          => $courseType,
            'categoryID'    => $courseCategory,
            'authorID'      => 1
        ));
        $course->timestamps = false;
        $course->save();

        return view('courses.add',['success' => true, 'categories' => $categories]);
    }
}
