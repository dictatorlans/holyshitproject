<?php

Route::group(['prefix'=> 'auth'], function(){
    Route::get('login', 'Auth\AuthController@getLogin');
    Route::post('login', 'Auth\AuthController@postLogin');
    Route::get('logout', 'Auth\AuthController@getLogout');
    Route::get('register', 'Auth\AuthController@getRegister');
    Route::post('register', 'Auth\AuthController@postRegister');
});

Route::group(['middleware'=>'auth'], function(){
    Route::get('/admin', function() {
        return view('admin.index');
    });
});

Route::get('password/email', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail');
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');

Route::get('/', function () {
    return view('master');
});

Route::get('/dashboard', function() {
    return view('dashboard.index');
});

Route::controller('courses', 'CoursesController');
Route::resource('courses', 'CoursesController');
