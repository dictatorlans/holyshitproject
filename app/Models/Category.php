<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

    /**
     * Таблица, с которой работает модель
     *
     * @var string
     */
    protected $table = 'categories';

    /**
     * Поля для массового заполнения
     *
     * @var array
     */
    protected $fillable = ['name', 'description'];

    /**
     * Поля запрещенные к заполнению
     *
     * @var array
     */
    protected $guarded = ['id'];
}
