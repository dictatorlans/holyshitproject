<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{

    /**
     * Таблица, с которой работает модель
     *
     * @var string
     */
    protected $table = 'courses';

    /**
     * Поля для массового заполнения
     *
     * @var array
     */
    protected $fillable = ['name', 'description', 'cost', 'type', 'adv_cost', 'categoryID','authorID'];

    /**
     * Поля запрещенные к заполнению
     *
     * @var array
     */
    protected $guarded = ['id'];

}
