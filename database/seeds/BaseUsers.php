<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;

class BaseUsers extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //User::create();
        DB::table('users')->insert([
            'username' => 'first_user',
            'email' => 'first@m.ru',
            'password' => bcrypt('first_password')
        ]);
    }
}
