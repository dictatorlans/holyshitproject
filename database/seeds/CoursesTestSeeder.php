<?php

use Illuminate\Database\Seeder;
use App\Models\Course;

class CoursesTestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $courses = new Course(array(
            'name'          => 'TestCourse',
            'description'   => 'Здесь могла быть ваша реклама.',
            'cost'          => 0.00,
            'type'          => 'free',
            'categoryID'    => 1,
            'authorID'      => 1
        ));
        $courses->timestamps = false;
        $courses->save();
    }
}
