<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCorpTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('corp', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('username', 40);
            $table->string('password', 32);
            $table->string('phone', 45);
            $table->string('email', 100);
            $table->string('name', 255);
            $table->text('address');
            //индексы
            $table->unique('username');
            $table->unique('email');
            $table->unsignedInteger('tarifID');
        });

        Schema::table('corp', function($table) {
            $table->foreign('tarifID')
                ->references('id')->on('tarif')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('corp', function(Blueprint $table){
            $table->dropForeign('tarif');
            $table->dropPrimary('id');
            $table->dropUnique('username');
            $table->dropUnique('email');
            $table->dropTimestamps();
        });
    }
}
