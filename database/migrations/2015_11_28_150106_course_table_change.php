<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CourseTableChange extends Migration
{
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            //категории курсов
            $table->increments('id');
            $table->text('name');
            $table->text('description');
        });
        Schema::table('courses', function (Blueprint $table) {
            $table->enum('type', ['free', 'paid']);      //тип курса - платный/бесплатный,
            $table->double('adv_cost', 6, 2);            //цена со скидкой
            $table->unsignedInteger('categoryID');       //ссылка на категорию
            $table->foreign('categoryID')
                ->references('id')->on('categories')
                ->onDelete('cascade');
        });
    }


    public function down()
    {
        Schema::table('courses', function (Blueprint $table) {
            $table->dropForeign('categoryID');
            $table->dropColumn(['adv_cost', 'type', 'categoryID']);
        });
        Schema::dropIfExists('categories');
    }
}
