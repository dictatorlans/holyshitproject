<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UsersTableChange extends Migration
{
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            //текстовый статус пользователя (как в соц сетях)
            $table->string('user_status', 255);
        });
    }
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn(['user_status']);
        });
    }
}
