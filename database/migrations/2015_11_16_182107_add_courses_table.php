<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->increments('id');
            $table->text('name');
            $table->text('description');
            $table->unsignedInteger('authorID');
            $table->double('cost', 6, 2);
        });
        Schema::table('courses', function(Blueprint $table) {
            $table->foreign('authorID')
                ->references('id')->on('corp')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('courses', function(Blueprint $table) {
            $table->dropForeign('authorID');
            $table->dropPrimary('id');
        });
    }
}
