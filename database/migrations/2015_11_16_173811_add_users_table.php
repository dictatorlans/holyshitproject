<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('username', 40);
            $table->string('password', 32);
            $table->string('phone', 45);
            $table->string('email', 100);
            $table->string('firstName', 40);
            $table->string('middleName', 40);
            $table->string('lastName', 40);
            $table->unsignedInteger('corpID');
            $table->unique('username');
            $table->unique('email');
        });

        Schema::table('users', function($table) {
            $table->foreign('corpID')
                ->references('id')->on('corp')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users', function (Blueprint $table) {
            $table->dropForeign('corpID');
            $table->dropPrimary('id');
            $table->dropUnique('username');
            $table->dropUnique('email');
            $table->dropTimestamps();
        });
    }
}
