<!DOCTYPE html>
<html>
    <head>
        <title>@yield('title')</title>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        @section('styles')
            <link rel="stylesheet" href="dashboard_style/style.css"/>
            <link rel="stylesheet" href="admin_style/font-awesome/css/font-awesome.min.css"/>
        @show
        <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
        <script type="text/javascript">
            $(document).on("scroll",function(){
                    if ($(document).scrollTop()> 60 && $("#fixed_block").hasClass("default") ){
                        $("#fixed_block").removeClass("default");
                        $("#fixed_block").addClass("fixed");
                        $(".logo").addClass("logo2");
                        $(".fixed_menu").addClass("fixed_menu2");
                    } else if($(document).scrollTop() <= 60 && $("#fixed_block").hasClass("fixed")) {
                        $("#fixed_block").removeClass("fixed");
                        $("#fixed_block").addClass("default");
                        $(".logo").removeClass("logo2");
                        $(".fixed_menu").removeClass("fixed_menu2");
                    }
            });
        </script>
    </head>
    <body>
    <div class="">
        <div>
            @include('dashboard.layouts.header')
        </div>
        <div class="">
            @include('dashboard.layouts.main')
        </div>
        <div class="">
            @include('dashboard.layouts.footer')
        </div>
    </div>
        @section('scripts')
            <script src="assets/js/modernizr-2.8.3.min.js"></script>
        @show
    </body>
</html>
