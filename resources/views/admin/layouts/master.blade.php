<!DOCTYPE html>
<html>
    <head>
        <title>@yield('title')</title>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">'
        <meta name="csrf-token" id="csrf-token" content="{{ csrf_token() }}">
        @section('styles')
            <link rel="stylesheet" href="admin_style/style.css"/>
            <link rel="stylesheet" href="admin_style/font-awesome/css/font-awesome.min.css"/>
        @show
    </head>
    <body>
    <div class="menu">
        @include('admin.layouts.menu')
    </div>
    <div class="main_content">
        @include('admin.layouts.header')
        <div class="main_content2">
                @include('admin.layouts.main')
                @include('admin.layouts.footer')
        </div>
    </div>
        @section('scripts')
            <script src="assets/js/modernizr-2.8.3.min.js"></script>
        @show
    </body>
</html>
