<header class="fix">
<div class="container">
<div class="row">
<div class="col-md-3 col-sm-3 col-xs-12">
<h2 id="logo">
<a href="http://themes.vibethemes.com/wplms/skins/modern/">
<img src="http://themes.vibethemes.com/wplms/skins/modern/wp-content/themes/wplms/images/logo.png" alt="WPLMS Modern theme">
<span>
<img src="http://themes.vibethemes.com/wplms/skins/modern/wp-content/uploads/2015/09/logo_black.png" alt="WPLMS Modern theme">
</span></a>
</h2>                    </div>
<div class="col-md-9 col-sm-9">
<div id="searchicon"><i class="icon-search-2"></i></div>
<div id="searchdiv" style="display: none;">
<form role="search" method="get" id="searchform" action="http://themes.vibethemes.com/wplms/skins/modern/">
<div><label class="screen-reader-text" for="s">Search for:</label>
<input type="text" value="" name="s" id="s" placeholder="Hit enter to search...">
<input type="submit" id="searchsubmit" value="Search">
</div>
</form>
</div>
<nav class="menu-main-menu-container"><ul id="menu-main-menu" class="menu"><li id="main-menu-item-2028" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children hasmenu"><a href="http://themes.vibethemes.com/wplms/skins/modern/all-courses/"><strong>All Courses</strong></a>
<ul class="sub-menu">
<li id="main-menu-item-1992" class="menu-item menu-item-type-post_type menu-item-object-course"><a href="http://themes.vibethemes.com/wplms/skins/modern/course/software-training/">Single Course</a></li>
<li id="main-menu-item-1987" class="menu-item menu-item-type-custom menu-item-object-custom"><a href="http://themes.vibethemes.com/wplms/skins/modern/quiz/book3-quiz/">Quiz</a></li>
<li id="main-menu-item-2032" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://themes.vibethemes.com/wplms/skins/modern/edit-course/">Edit Course</a></li>
<li id="main-menu-item-2031" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://themes.vibethemes.com/wplms/skins/modern/certificate/">Certificate</a></li>
</ul>
</li>
<li id="main-menu-item-2027" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children hasmenu"><a href="http://themes.vibethemes.com/wplms/skins/modern/page-builder-page/"><strong>Pages</strong></a>
<ul class="sub-menu">
<li id="main-menu-item-2118" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://themes.vibethemes.com/wplms/skins/modern/activity/">Activity</a></li>
<li id="main-menu-item-2251" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://themes.vibethemes.com/wplms/skins/modern/members/">Members</a></li>
<li id="main-menu-item-2120" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://themes.vibethemes.com/wplms/skins/modern/groups/">Groups</a></li>
<li id="main-menu-item-2002" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children hasmenu"><a href="http://themes.vibethemes.com/wplms/skins/modern/page-builder-page/">Page Builder Page</a>
<ul class="sub-menu">
<li id="main-menu-item-1997" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://themes.vibethemes.com/wplms/skins/modern/about/">About</a></li>
<li id="main-menu-item-2004" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://themes.vibethemes.com/wplms/skins/modern/services/">Services</a></li>
</ul>
</li>
</ul>
</li>
<li id="main-menu-item-2000" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://themes.vibethemes.com/wplms/skins/modern/blog/"><strong>Blog</strong></a></li>
<li id="main-menu-item-2001" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://themes.vibethemes.com/wplms/skins/modern/contact-us/"><strong>Contact Us</strong></a></li>
</ul></nav>
</div>
<div id="vibe_bp_login" style="display: none;">
<div class="widget vibe-bp-login">

<form name="login-form" id="vbp-login-form" class="standard-form" action="" method="post">
<label>Username<br>
<input type="text" name="log" id="side-user-login" class="input" tabindex="1" value=""></label>

<label>Password <a href="" tabindex="5" class="tip" title="" data-original-title="Forgot Password"><i class="icon-question"></i></a><br>
<input type="password" tabindex="2" name="pwd" id="sidebar-user-pass" class="input" value=""></label>

<p class=""><label><input name="rememberme" tabindex="3" type="checkbox" id="sidebar-rememberme" value="forever">Remember Me</label></p>

<input type="submit" name="user-submit" id="sidebar-wp-submit" value="Log In" tabindex="100">
<input type="hidden" name="user-cookie" value="1">
<a href="" class="vbpregister" title="Create an account" tabindex="5">Sign Up</a>           				</form>
</div></div></div></div>
</header>