<main>
<div id="global" class="global">
@include('layouts.sidebar')
<div class="pusher">
@include('layouts.headertop')
@include('layouts.header')

<section id="content">
<div class="container">
<div class="vibe_editor clearfix"></div>
</div>
</section>
<section class="stripe">
<!-- Begin Stripe stripe fullwidth -->
<div class="v_module v_column stripe fullwidth v_first" style="transform: translateY(0px);">
<style>#parallax261 {
background: url(http://themes.vibethemes.com/wplms/skins/modern/wp-content/uploads/2015/09/student_college.jpg) 50% -0px;
position:relative;background-size: cover;
min-height:100vh;
} #parallax261 .parallax_content{
padding-top:200px;
padding-bottom:60px;
}.homesearch{text-align:center;color:#FFF;}
.homesearch h1{
color:#fff;
font-size: 48px;
font-weight: 600;
margin: 0;
text-transform: uppercase;
}
.homesearch h4{color:#fff;font-size:32px;font-weight:600;}
.homesearch input#s {
background: rgba(0,0,0,0.5);
border-radius: 5px;
color: #FFF;
font-size: 24px;
text-align: center;
border-color: rgba(255,255,255,0.2);
}
.homesearch form.left{float:none;display:inline-block;}
.homesearch .three_fifth .column_content{
background:rgba(0,0,0,0.5);
padding:60px 10px 30px;
}
.homesearch .column_content{
display:inline_block;
text-align:center;
}
.homesearch .three_fifth .one_third .column_content{background:none;padding:0;}
#searchsubmit{display:none;}
</style><div id="parallax261" data-rev="2" data-scroll="2" data-height="100vh" data-adjust="0" class="v_module v_parallax_block  homesearch">
<div class="parallax_content"><div id="Parallax_Title"></div><p></p><div class="one_fifth "><div class="column_content "><p></p>
<p></p></div></div><div class="three_fifth "><div class="column_content "><p></p>
<h1 style="color: #009dd8;"><strong>Online Education.</strong></h1>
<h1>Learn skills online.</h1>
<p></p><form role="search" method="get" class="left" action="http://themes.vibethemes.com/wplms/skins/modern/">
<input type="hidden" name="post_type" value="course">
<input type="text" value="" name="s" id="s" placeholder="Type Keywords..">
<input type="submit" id="searchsubmit" value="Search"><p></p>
<p></p><div class="one_third "><div class="column_content "><p></p>
<h4><div class="numscroller roller-title-number-1 scrollzip isShown" data-max="11" data-min="0" data-delay="0" data-increment="1" data-slno="1">11</div></h4>
<p>Courses</p>
<p></p></div></div><div class="one_third "><div class="column_content "><p></p>
<h4><div class="numscroller roller-title-number-2 scrollzip isShown" data-max="1" data-min="0" data-delay="0" data-increment="1" data-slno="2">1</div></h4>
<p>Instructors</p>
<p></p></div></div><div class="one_third "><div class="column_content "><p></p>
<h4><div class="numscroller roller-title-number-3 scrollzip isShown" data-max="263" data-min="0" data-delay="0" data-increment="1" data-slno="3">263</div></h4>
<p>Students</p>
<p></p></div></div></form></div></div><div class="one_fifth "><div class="column_content "><p></p>
<p></p></div></div><p></p></div></div>
</div>
<!-- End Stripestripe fullwidth -->
</section>
<section class="main">
<div class="container">
<div class="full-width">
<div class="vibe_editor clearfix">
</div></div>
</div></section>
<section class="stripe homeblocks">
<div class="container">
<!-- Begin Stripe stripe_container fullwidth -->
<div class="v_module v_column stripe_container fullwidth v_first">
<style>.homeblocks.stripe{background:#fff;padding:90px 0;}
.homeblocks .col-md-4 img{
max-width:150px;
height:auto;
margin-bottom:15px;
}</style><div class="v_module v_text_block " data-class="homeblocks"><p style="text-align: center;"></p><div class="v_module v_column col-md-4 col-sm-4 "><p></p><p style="text-align: center;"><img class="alignnone size-full wp-image-2125" src="http://themes.vibethemes.com/wplms/skins/modern/wp-content/uploads/2014/02/ladder.png" alt="ladder" width="150" height="150"></p><p style="text-align: center;"></p><h3 class="heading center"><span>Quizzes and Assignments</span></h3><p></p><p style="text-align: center;">Integrated Quizzes with 8 question types and upload type&nbsp;assignments.</p><p style="text-align: center;"></p></div> <!-- end .v_column_col-md-4 col-sm-4 --><div class="v_module v_column col-md-4 col-sm-4 "><p></p><p style="text-align: center;"><img class="alignnone size-full wp-image-2126" src="http://themes.vibethemes.com/wplms/skins/modern/wp-content/uploads/2014/02/study.png" alt="study" width="150" height="150"></p><p style="text-align: center;"></p><h3 class="heading center"><span>Learning Paths</span></h3><p></p><p style="text-align: center;">Define learning paths for students. Award badges on achievements.</p><p style="text-align: center;"></p></div> <!-- end .v_column_col-md-4 col-sm-4 --><div class="v_module v_column col-md-4 col-sm-4 "><p></p><p style="text-align: center;"><img class="alignnone size-full wp-image-2127" src="http://themes.vibethemes.com/wplms/skins/modern/wp-content/uploads/2014/02/book.png" alt="book" width="150" height="150"></p><p style="text-align: center;"></p><h3 class="heading center"><span>Track Activity</span></h3><p></p><p style="text-align: center;">Track every activity of your students. Check their performance and graphs.</p><p style="text-align: center;"></p></div> <!-- end .v_column_col-md-4 col-sm-4 --><p></p></div>
</div>
<!-- End Stripestripe_container fullwidth -->
</div>
</section>
<section class="main nextstripe">
<div class="container">
<div class="full-width">
<div class="vibe_editor clearfix">
</div>
</div>
</div></section>
<section class="stripe">
<!-- Begin Stripe stripe fullwidth -->
<div class="v_module v_column stripe fullwidth v_first">
<style>.homemainblocks {margin-top:-10px;}
.homemainblocks .v_column.col-md-3{
padding:30px;
background:#008ABE;
color:#FFF;letter-spacing:1px;
line-height:1.8;
font-size:14px;
min-height:360px;
}
.homemainblocks h3.heading.white{color:#FFF;letter-spacing:2px;margin-bottom:15px;}
.homemainblocks h3.heading.white:after{
background:#FFF;
}
.homemainblocks .v_column.col-md-3:nth-child(2){
background:#00A6E5;
}
.homemainblocks .v_column.col-md-3:nth-child(3){
background:#00B9FF;
}
.homemainblocks .v_column.col-md-3:nth-child(4){
background:#0093CB;
}
</style><div class="v_module v_text_block  homemainblocks"><p></p><div class="v_module v_column col-md-3 col-sm-6 "><p></p><p><style> #icon1078{font-size:44px;}
</style><i class="icon-college-24" id="icon1078"></i></p><p></p><h3 class="heading white"><span>Online courses</span></h3><p></p><p>Create unlimited courses using the easy to use front end course creation interface.</p><p></p></div> <!-- end .v_column_col-md-3 col-sm-6 --><div class="v_module v_column col-md-3 col-sm-6 "><p></p><p><style> #icon8893{font-size:44px;}
</style><i class="icon-town-hall-24" id="icon8893"></i></p><p></p><h3 class="heading white"><span>Student Management</span></h3><p></p><p>Student management for every course.&nbsp;Manage courses and students using easy to use interface.</p><p></p></div> <!-- end .v_column_col-md-3 col-sm-6 --><div class="v_module v_column col-md-3 col-sm-6 "><p></p><p><style> #icon8277{font-size:44px;}
</style><i class="icon-certificate-file" id="icon8277"></i></p><p></p><h3 class="heading white"><span>Award Certificates</span></h3><p></p><p>Award passing certificates to students. Validate certificates using certificate codes.</p><p></p></div> <!-- end .v_column_col-md-3 col-sm-6 --><div class="v_module v_column col-md-3 col-sm-6 "><p></p><p><style> #icon7846{font-size:44px;}
</style><i class="icon-sheriff-badge" id="icon7846"></i></p><p></p><h3 class="heading white"><span>Award Badges</span></h3><p></p><p>Award excellence badges to deserving students in course. Integrate with Mozilla open Badges.</p><p></p></div> <!-- end .v_column_col-md-3 col-sm-6 --><p></p></div>
</div>
<!-- End Stripestripe fullwidth -->
</section>
<section class="main">
<div class="container">
<div class="full-width">
<div class="vibe_editor clearfix">
<div class="v_module v_column col-md-6 col-sm-6 v_first">
<style>.featureblock{margin:90px 0 30px;text-align:center;}
.featureblock li{padding:6px 0;}</style><div class="v_module v_text_block v_first featureblock"><p><img class="alignnone size-medium wp-image-2078 zoom animate load" src="http://themes.vibethemes.com/wplms/skins/modern/wp-content/uploads/2014/02/tutorials-460x369.png" alt="tutorials" width="300" height="241"></p></div>
</div> <!-- end .v_column_col-md-6 col-sm-6 -->
<div class="v_module v_column col-md-6 col-sm-6 ">
<div class="v_module v_text_block  featureblock"><p style="text-align: left;"></p><h3 class="heading "><span>E-Learning Portal</span></h3><p></p><p style="text-align: left;">With wplms you get a lot powerful features yo start your online business.</p><ul><li style="text-align: left;">
<i class="icon-check-alt zoom animate load" id="icon9198"></i> Create courses online</li><li style="text-align: left;">
<i class="icon-check-alt zoom animate load" id="icon6263"></i> Sell courses online</li><li style="text-align: left;">
<i class="icon-check-alt zoom animate load" id="icon2025"></i> Earn commissions online</li><li style="text-align: left;">
<i class="icon-check-alt zoom animate load" id="icon6404"></i> Filters and search courses online</li><li style="text-align: left;">
<i class="icon-check-alt zoom animate load" id="icon8716"></i> Drip content, Unit specific drip, Section-wise drip</li><li style="text-align: left;">
<i class="icon-check-alt zoom animate load" id="icon4940"></i> Course start date, Course Seats</li><li style="text-align: left;">
<i class="icon-check-alt zoom animate load" id="icon4510"></i> Unit Start date and time</li></ul></div>
</div> <!-- end .v_column_col-md-6 col-sm-6 -->
</div>
</div>
</div></section>
<section class="stripe" style="height: 480px;">
<!-- Begin Stripe stripe fullwidth -->
<div class="v_module v_column stripe fullwidth v_first">
<style>#parallax600 {
background: url(http://themes.vibethemes.com/wplms/skins/modern/wp-content/uploads/2015/09/talent.jpg) 50% -0px;
position:relative;background-size: cover;
min-height:480px;
} #parallax600 .parallax_content{
padding-top:100px;
padding-bottom:100px;
}
</style><div id="parallax600" data-rev="1" data-scroll="1" data-height="480" data-adjust="0" class="v_module v_parallax_block " style="background-position: 50% 0px;">
<div class="parallax_content"><div id="Parallax_Title"></div><h2 style="text-align: center;"><span style="font-size: 48px;">Get started with Online Education !</span></h2>
<p style="text-align: center;"><span style="font-size: 20px;"><strong><span style="color: #999999;">Reach more users, generate more content, earn more commissions</span> !</strong></span></p>
<p style="text-align: center;"><style> #button3000{} #button3000:hover{}</style><a target="_self" id="button3000" class="button base" href="#">Learn More</a> <style> #button6807{} #button6807:hover{}</style><a target="_self" id="button6807" class="button primary" href="#">Get Started</a></p></div></div>
</div>
<!-- End Stripestripe fullwidth -->
</section>
<section class="main">
<div class="container">
<div class="full-width">
<div class="vibe_editor clearfix">
</div></div>
</div></section>
<section class="stripe infinite_load_courses">
<div class="container">
<!-- Begin Stripe stripe_container fullwidth -->
<div class="v_module v_column stripe_container fullwidth v_first">
<style>.infinite_load_courses.stripe{margin-top:60px;}.grid.masonry li .block { margin-bottom:30px;}</style><div class="v_module vibe_post_grid " data-class="infinite_load_courses"><div id="Heading"></div><div class="vibe_grid  inifnite_scroll masonry loaded" data-page="2"><div class="wp_query_args" data-max-pages="2">{"title":"Heading","show_title":"0","post_type":"course","taxonomy":"","term":"","post_ids":"","course_style":"rated","featured_style":"modern1","masonry":"1","grid_columns":" grid-item","column_width":"262","gutter":"30","grid_number":"6","infinite":"1","pagination":"0","grid_excerpt_length":"100","grid_lightbox":"0","grid_link":"0","css_class":"","container_css":"infinite_load_courses","custom_css":".infinite_load_courses.stripe{margin-top:60px;}.grid.masonry li .block { margin-bottom:30px;}"}</div><ul class="grid masonry" data-width="262" data-gutter="30" style="position: relative; height: 1044px;"><li class="grid-item masonry-brick" style="width: 262px; position: absolute; top: 0px; left: 0px;"><div class="block modern_course" data-id="1216"><div class="block_media"><a href="http://themes.vibethemes.com/wplms/skins/modern/course/starting-a-startup/"></a><a href="http://themes.vibethemes.com/wplms/skins/modern/course/starting-a-startup/"><img width="300" height="180" src="http://themes.vibethemes.com/wplms/skins/modern/wp-content/uploads/2015/09/course5-460x276.jpg" class="attachment-medium wp-post-image" alt="course5"></a></div><div class="block_content"><h4 class="block_title"><a href="http://themes.vibethemes.com/wplms/skins/modern/course/starting-a-startup/" title="Starting a Startup">Starting a Startup</a></h4><div class="course_price"><strong itemprop="offers" itemscope="" itemtype="http://schema.org/Offer"><span class="amount" itemprop="price">£19.00</span></strong></div><a href="http://themes.vibethemes.com/wplms/skins/modern/members/jorge/" class="modern_course_instructor" title="Course Author Eddard Stark"><img src="http://themes.vibethemes.com/wplms/skins/modern/wp-content/uploads/avatars/1/cc0248098d207038a1b62aecd6d3d9e1-bpthumb.jpg" class="avatar user-1-avatar avatar-64 photo" width="64" height="64" alt="Profile Photo"></a><div class="course_meta"><span class="dashicons dashicons-groups">6</span> <div class="modern-star-rating"><span class="dashicons dashicons-star-filled"></span><span class="dashicons dashicons-star-filled"></span><span class="dashicons dashicons-star-filled"></span><span class="dashicons dashicons-star-filled"></span><span class="dashicons dashicons-star-filled"></span></div></div></div></div></li><li class="grid-item masonry-brick" style="width: 262px; position: absolute; top: 0px; left: 292px;"><div class="block modern_course" data-id="1242"><div class="block_media"><a href="http://themes.vibethemes.com/wplms/skins/modern/course/how-to-write-effectively/"></a><a href="http://themes.vibethemes.com/wplms/skins/modern/course/how-to-write-effectively/"><img width="300" height="180" src="http://themes.vibethemes.com/wplms/skins/modern/wp-content/uploads/2015/09/course1-460x276.jpg" class="attachment-medium wp-post-image" alt="course1"></a></div><div class="block_content"><h4 class="block_title"><a href="http://themes.vibethemes.com/wplms/skins/modern/course/how-to-write-effectively/" title="How to write effectively">How to write effectively</a></h4><div class="course_price"><strong itemprop="offers" itemscope="" itemtype="http://schema.org/Offer"><span class="amount" itemprop="price">£20.00</span><span class="subs"> per  month</span></strong></div><a href="http://themes.vibethemes.com/wplms/skins/modern/members/jorge/" class="modern_course_instructor" title="Course Author Eddard Stark"><img src="http://themes.vibethemes.com/wplms/skins/modern/wp-content/uploads/avatars/1/cc0248098d207038a1b62aecd6d3d9e1-bpthumb.jpg" class="avatar user-1-avatar avatar-64 photo" width="64" height="64" alt="Profile Photo"></a><div class="course_meta"><span class="dashicons dashicons-groups">1</span> <div class="modern-star-rating"><span class="dashicons dashicons-star-filled"></span><span class="dashicons dashicons-star-filled"></span><span class="dashicons dashicons-star-filled"></span><span class="dashicons dashicons-star-filled"></span><span class="dashicons dashicons-star-half"></span></div></div></div></div></li><li class="grid-item masonry-brick" style="width: 262px; position: absolute; top: 0px; left: 584px;"><div class="block modern_course" data-id="1260"><div class="block_media"><a href="http://themes.vibethemes.com/wplms/skins/modern/course/software-training/"></a><a href="http://themes.vibethemes.com/wplms/skins/modern/course/software-training/"><img width="300" height="180" src="http://themes.vibethemes.com/wplms/skins/modern/wp-content/uploads/2015/09/course9-460x276.jpg" class="attachment-medium wp-post-image" alt="course9"></a></div><div class="block_content"><h4 class="block_title"><a href="http://themes.vibethemes.com/wplms/skins/modern/course/software-training/" title="Software Training">Software Training</a></h4><div class="course_price"><strong itemprop="offers" itemscope="" itemtype="http://schema.org/Offer"><del><span class="amount" itemprop="price">£29.00</span></del> <ins><span class="amount" itemprop="price">£19.00</span></ins><span class="subs"> per  month</span></strong></div><a href="http://themes.vibethemes.com/wplms/skins/modern/members/jorge/" class="modern_course_instructor" title="Course Author Eddard Stark"><img src="http://themes.vibethemes.com/wplms/skins/modern/wp-content/uploads/avatars/1/cc0248098d207038a1b62aecd6d3d9e1-bpthumb.jpg" class="avatar user-1-avatar avatar-64 photo" width="64" height="64" alt="Profile Photo"></a><div class="course_meta"><span class="dashicons dashicons-groups">15</span> <div class="modern-star-rating"><span class="dashicons dashicons-star-filled"></span><span class="dashicons dashicons-star-filled"></span><span class="dashicons dashicons-star-filled"></span><span class="dashicons dashicons-star-filled"></span><span class="dashicons dashicons-star-empty"></span></div></div></div></div></li><li class="grid-item masonry-brick" style="width: 262px; position: absolute; top: 0px; left: 876px;"><div class="block modern_course" data-id="1188"><div class="block_media"><a href="http://themes.vibethemes.com/wplms/skins/modern/course/how-to-become-a-powerful-speaker/"></a><a href="http://themes.vibethemes.com/wplms/skins/modern/course/how-to-become-a-powerful-speaker/"><img width="300" height="180" src="http://themes.vibethemes.com/wplms/skins/modern/wp-content/uploads/2015/09/course6-460x276.jpg" class="attachment-medium wp-post-image" alt="course6"></a></div><div class="block_content"><h4 class="block_title"><a href="http://themes.vibethemes.com/wplms/skins/modern/course/how-to-become-a-powerful-speaker/" title="How to become a Powerful Speaker">How to become a Powerful Speaker</a></h4><div class="course_price"><strong itemprop="offers" itemscope="" itemtype="http://schema.org/Offer"><span class="amount" itemprop="price">£19.00</span><span class="subs"> per  week</span></strong></div><a href="http://themes.vibethemes.com/wplms/skins/modern/members/jorge/" class="modern_course_instructor" title="Course Author Eddard Stark"><img src="http://themes.vibethemes.com/wplms/skins/modern/wp-content/uploads/avatars/1/cc0248098d207038a1b62aecd6d3d9e1-bpthumb.jpg" class="avatar user-1-avatar avatar-64 photo" width="64" height="64" alt="Profile Photo"></a><div class="course_meta"><span class="dashicons dashicons-groups">1</span> <div class="modern-star-rating"><span class="dashicons dashicons-star-filled"></span><span class="dashicons dashicons-star-filled"></span><span class="dashicons dashicons-star-filled"></span><span class="dashicons dashicons-star-filled"></span><span class="dashicons dashicons-star-empty"></span></div></div></div></div></li><li class="grid-item masonry-brick" style="width: 262px; position: absolute; top: 348px; left: 0px;"><div class="block modern_course" data-id="1204"><div class="block_media"><a href="http://themes.vibethemes.com/wplms/skins/modern/course/social-media-management/"></a><a href="http://themes.vibethemes.com/wplms/skins/modern/course/social-media-management/"><img width="300" height="180" src="http://themes.vibethemes.com/wplms/skins/modern/wp-content/uploads/2015/09/course9-460x276.jpg" class="attachment-medium wp-post-image" alt="course9"></a></div><div class="block_content"><h4 class="block_title"><a href="http://themes.vibethemes.com/wplms/skins/modern/course/social-media-management/" title="Social Media Management">Social Media Management</a></h4><div class="course_price"><strong itemprop="offers" itemscope="" itemtype="http://schema.org/Offer"><span class="amount" itemprop="price">£49.00</span></strong></div><a href="http://themes.vibethemes.com/wplms/skins/modern/members/jorge/" class="modern_course_instructor" title="Course Author Eddard Stark"><img src="http://themes.vibethemes.com/wplms/skins/modern/wp-content/uploads/avatars/1/cc0248098d207038a1b62aecd6d3d9e1-bpthumb.jpg" class="avatar user-1-avatar avatar-64 photo" width="64" height="64" alt="Profile Photo"></a><div class="course_meta"><span class="dashicons dashicons-groups">0</span> <div class="modern-star-rating"><span class="dashicons dashicons-star-filled"></span><span class="dashicons dashicons-star-filled"></span><span class="dashicons dashicons-star-filled"></span><span class="dashicons dashicons-star-filled"></span><span class="dashicons dashicons-star-empty"></span></div></div></div></div></li><li class="grid-item masonry-brick" style="width: 262px; position: absolute; top: 348px; left: 292px;"><div class="block modern_course" data-id="1235"><div class="block_media"><a href="http://themes.vibethemes.com/wplms/skins/modern/course/basic-of-nature-photography/"></a><a href="http://themes.vibethemes.com/wplms/skins/modern/course/basic-of-nature-photography/"><img width="300" height="180" src="http://themes.vibethemes.com/wplms/skins/modern/wp-content/uploads/2015/09/course2-460x276.jpg" class="attachment-medium wp-post-image" alt="course2"></a></div><div class="block_content"><h4 class="block_title"><a href="http://themes.vibethemes.com/wplms/skins/modern/course/basic-of-nature-photography/" title="Basic of Nature Photography">Basic of Nature Photography</a></h4><div class="course_price"><strong itemprop="offers" itemscope="" itemtype="http://schema.org/Offer"><del><span class="amount" itemprop="price">£29.00</span></del> <ins><span class="amount" itemprop="price">£19.00</span></ins><span class="subs"> per  week</span></strong></div><a href="http://themes.vibethemes.com/wplms/skins/modern/members/jorge/" class="modern_course_instructor" title="Course Author Eddard Stark"><img src="http://themes.vibethemes.com/wplms/skins/modern/wp-content/uploads/avatars/1/cc0248098d207038a1b62aecd6d3d9e1-bpthumb.jpg" class="avatar user-1-avatar avatar-64 photo" width="64" height="64" alt="Profile Photo"></a><div class="course_meta"><span class="dashicons dashicons-groups">3</span> <div class="modern-star-rating"><span class="dashicons dashicons-star-filled"></span><span class="dashicons dashicons-star-filled"></span><span class="dashicons dashicons-star-filled"></span><span class="dashicons dashicons-star-filled"></span><span class="dashicons dashicons-star-empty"></span></div></div></div></div></li><li class="grid-item masonry-brick" style="width: 262px; position: absolute; top: 348px; left: 584px;"><div class="block modern_course" data-id="1188"><div class="block_media"><a href="http://themes.vibethemes.com/wplms/skins/modern/course/how-to-become-a-powerful-speaker/"></a><a href="http://themes.vibethemes.com/wplms/skins/modern/course/how-to-become-a-powerful-speaker/"><img width="300" height="180" src="http://themes.vibethemes.com/wplms/skins/modern/wp-content/uploads/2015/09/course6-460x276.jpg" class="attachment-medium wp-post-image" alt="course6"></a></div><div class="block_content"><h4 class="block_title"><a href="http://themes.vibethemes.com/wplms/skins/modern/course/how-to-become-a-powerful-speaker/" title="How to become a Powerful Speaker">How to become a Powerful Speaker</a></h4><div class="course_price"><strong itemprop="offers" itemscope="" itemtype="http://schema.org/Offer"><span class="amount" itemprop="price">£19.00</span><span class="subs"> per  week</span></strong></div><a href="http://themes.vibethemes.com/wplms/skins/modern/members/jorge/" class="modern_course_instructor" title="Course Author Eddard Stark"><img src="http://themes.vibethemes.com/wplms/skins/modern/wp-content/uploads/avatars/1/cc0248098d207038a1b62aecd6d3d9e1-bpthumb.jpg" class="avatar user-1-avatar avatar-64 photo" width="64" height="64" alt="Profile Photo"></a><div class="course_meta"><span class="dashicons dashicons-groups">1</span> <div class="modern-star-rating"><span class="dashicons dashicons-star-filled"></span><span class="dashicons dashicons-star-filled"></span><span class="dashicons dashicons-star-filled"></span><span class="dashicons dashicons-star-filled"></span><span class="dashicons dashicons-star-empty"></span></div></div></div></div></li><li class="grid-item masonry-brick" style="width: 262px; position: absolute; top: 348px; left: 876px;"><div class="block modern_course" data-id="1176"><div class="block_media"><a href="http://themes.vibethemes.com/wplms/skins/modern/course/digital-photography-2/"></a><a href="http://themes.vibethemes.com/wplms/skins/modern/course/digital-photography-2/"><img width="300" height="180" src="http://themes.vibethemes.com/wplms/skins/modern/wp-content/uploads/2015/09/course3-460x276.jpg" class="attachment-medium wp-post-image" alt="course3"></a></div><div class="block_content"><h4 class="block_title"><a href="http://themes.vibethemes.com/wplms/skins/modern/course/digital-photography-2/" title="Digital Photography">Digital Photography</a></h4><div class="course_price"><strong itemprop="offers" itemscope="" itemtype="http://schema.org/Offer"><span class="amount" itemprop="price">£29.00</span><span class="subs"> per 2 weeks</span></strong></div><a href="http://themes.vibethemes.com/wplms/skins/modern/members/jorge/" class="modern_course_instructor" title="Course Author Eddard Stark"><img src="http://themes.vibethemes.com/wplms/skins/modern/wp-content/uploads/avatars/1/cc0248098d207038a1b62aecd6d3d9e1-bpthumb.jpg" class="avatar user-1-avatar avatar-64 photo" width="64" height="64" alt="Profile Photo"></a><div class="course_meta"><span class="dashicons dashicons-groups">1</span> <div class="modern-star-rating"><span class="dashicons dashicons-star-empty"></span><span class="dashicons dashicons-star-empty"></span><span class="dashicons dashicons-star-empty"></span><span class="dashicons dashicons-star-empty"></span><span class="dashicons dashicons-star-empty"></span></div></div></div></div></li><li class="grid-item masonry-brick" style="width: 262px; position: absolute; top: 696px; left: 0px;"><div class="block modern_course" data-id="1155"><div class="block_media"><a href="http://themes.vibethemes.com/wplms/skins/modern/course/how-to-develop-mobile-apps/"></a><a href="http://themes.vibethemes.com/wplms/skins/modern/course/how-to-develop-mobile-apps/"><img width="300" height="180" src="http://themes.vibethemes.com/wplms/skins/modern/wp-content/uploads/2015/09/course8-460x276.jpg" class="attachment-medium wp-post-image" alt="course8"></a></div><div class="block_content"><h4 class="block_title"><a href="http://themes.vibethemes.com/wplms/skins/modern/course/how-to-develop-mobile-apps/" title="How to develop Mobile Apps">How to develop Mobile Apps</a></h4><div class="course_price"><strong itemprop="offers" itemscope="" itemtype="http://schema.org/Offer"><span class="amount" itemprop="price">£29.00</span></strong></div><a href="http://themes.vibethemes.com/wplms/skins/modern/members/jorge/" class="modern_course_instructor" title="Course Author Eddard Stark"><img src="http://themes.vibethemes.com/wplms/skins/modern/wp-content/uploads/avatars/1/cc0248098d207038a1b62aecd6d3d9e1-bpthumb.jpg" class="avatar user-1-avatar avatar-64 photo" width="64" height="64" alt="Profile Photo"></a><div class="course_meta"><span class="dashicons dashicons-groups">11</span> <div class="modern-star-rating"><span class="dashicons dashicons-star-filled"></span><span class="dashicons dashicons-star-filled"></span><span class="dashicons dashicons-star-filled"></span><span class="dashicons dashicons-star-empty"></span><span class="dashicons dashicons-star-empty"></span></div></div></div></div></li><li class="grid-item masonry-brick" style="width: 262px; position: absolute; top: 696px; left: 292px;"><div class="block modern_course" data-id="1139"><div class="block_media"><a href="http://themes.vibethemes.com/wplms/skins/modern/course/marketing-management/"></a><a href="http://themes.vibethemes.com/wplms/skins/modern/course/marketing-management/"><img width="300" height="180" src="http://themes.vibethemes.com/wplms/skins/modern/wp-content/uploads/2015/09/course4-460x276.jpg" class="attachment-medium wp-post-image" alt="course4"></a></div><div class="block_content"><h4 class="block_title"><a href="http://themes.vibethemes.com/wplms/skins/modern/course/marketing-management/" title="Marketing Management">Marketing Management</a></h4><div class="course_price"><strong itemprop="offers" itemscope="" itemtype="http://schema.org/Offer"><span class="amount" itemprop="price">£19.00</span><span class="subs"> per  month</span></strong></div><a href="http://themes.vibethemes.com/wplms/skins/modern/members/jorge/" class="modern_course_instructor" title="Course Author Eddard Stark"><img src="http://themes.vibethemes.com/wplms/skins/modern/wp-content/uploads/avatars/1/cc0248098d207038a1b62aecd6d3d9e1-bpthumb.jpg" class="avatar user-1-avatar avatar-64 photo" width="64" height="64" alt="Profile Photo"></a><div class="course_meta"><span class="dashicons dashicons-groups">3</span> <div class="modern-star-rating"><span class="dashicons dashicons-star-filled"></span><span class="dashicons dashicons-star-filled"></span><span class="dashicons dashicons-star-filled"></span><span class="dashicons dashicons-star-empty"></span><span class="dashicons dashicons-star-empty"></span></div></div></div></div></li><li class="grid-item masonry-brick" style="width: 262px; position: absolute; top: 696px; left: 584px;"><div class="block modern_course" data-id="1127"><div class="block_media"><a href="http://themes.vibethemes.com/wplms/skins/modern/course/step-by-step-macintosh-training/"></a><a href="http://themes.vibethemes.com/wplms/skins/modern/course/step-by-step-macintosh-training/"><img width="300" height="180" src="http://themes.vibethemes.com/wplms/skins/modern/wp-content/uploads/2015/09/course7-460x276.jpg" class="attachment-medium wp-post-image" alt="course7"></a></div><div class="block_content"><h4 class="block_title"><a href="http://themes.vibethemes.com/wplms/skins/modern/course/step-by-step-macintosh-training/" title="Step by Step Macintosh Training">Step by Step Macintosh Training</a></h4><div class="course_price"><strong itemprop="offers" itemscope="" itemtype="http://schema.org/Offer"><span class="amount" itemprop="price">£19.00</span><span class="subs"> per  month</span></strong></div><a href="http://themes.vibethemes.com/wplms/skins/modern/members/jorge/" class="modern_course_instructor" title="Course Author Eddard Stark"><img src="http://themes.vibethemes.com/wplms/skins/modern/wp-content/uploads/avatars/1/cc0248098d207038a1b62aecd6d3d9e1-bpthumb.jpg" class="avatar user-1-avatar avatar-64 photo" width="64" height="64" alt="Profile Photo"></a><div class="course_meta"><span class="dashicons dashicons-groups">1</span> <div class="modern-star-rating"><span class="dashicons dashicons-star-empty"></span><span class="dashicons dashicons-star-empty"></span><span class="dashicons dashicons-star-empty"></span><span class="dashicons dashicons-star-empty"></span><span class="dashicons dashicons-star-empty"></span></div></div></div></div></li></ul></div><div class="load_grid" style="display: none;"><span>Loading..</span></div>
<div class="end_grid" style="display: block;"><span>No more to load</span></div></div>
</div>
<!-- End Stripestripe_container fullwidth -->
</div>
</section>
<section class="main nextstripe">
<div class="container">
<div class="full-width">
<div class="vibe_editor clearfix">
</div></div>
</div></section>
<section class="stripe footerblock">
<div class="container">
<!-- Begin Stripe stripe_container fullwidth -->
<div class="v_module v_column stripe_container fullwidth v_first">
<style>.footerblock.stripe{background:#009dd8;color:#FFF;padding:30px 0 15px;}
.footerblock.stripe .button.border{margin-top:-30px;}</style><div class="v_module v_text_block " data-class="footerblock"><p></p><h3 class="heading white"><span>Get started with online education</span></h3><p></p><p><style> #button5752{} #button5752:hover{}</style><a target="_self" id="button5752" class="button border right" href="#">Get started now &nbsp;
<i class="icon-arrow-1-right zoom animate load" id="icon354"></i></a><strong>Online courses, create and sell courses, students, course stats, student stats, reports, import export and much more..</strong></p><p>&nbsp;</p></div>
</div>
<!-- End Stripestripe_container fullwidth -->
</div>
</section>
<section class="main nextstripe">
<div class="container">
<div class="full-width">
<div class="vibe_editor clearfix">
</div>        </div>
</div>
</section>

@include('layouts.footer')

</div></div>
</main>