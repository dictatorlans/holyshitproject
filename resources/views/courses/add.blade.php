@extends('internal')


@section('title')
Добавить курс
@endsection
@section('CategoryTitle')
Курсы
@endsection

@section('styles')
    @parent

@endsection

@section('content')
        <div class="container">
        <?
           if(isset($success))
           if($success) { ?>
                <div class="alert alert-success" role="alert">Курс успешно добавлен.</div>
           <? }
        ?>

        <form action="" method="post">
          <input name="author" type="hidden" value="1"/>
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <div class="form-group">
            <label for="CourseName">Название</label>
            <input type="text" class="form-control" id="CourseName" name="name" placeholder="Название курса">
          </div>
          <div class="form-group">
            <label for="CourseDescription">Описание</label>
            <textarea class="form-control" id="CourseDescription" name="descr" placeholder="Описание курса"></textarea>
          </div>
          <div class="form-group">
            <label for="CoursePrice">Стоимость</label>
            <input type="text" class="form-control" id="CoursePrice" name="price" placeholder="Стоимость курса">
          </div>
          <div class="form-group">
            <label for="CourseCategory">Категория</label>
            <select class="form-control" id="CourseCategory" name="category">
                @foreach($categories as $category)
                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                @endforeach
            </select>
          </div>
          <button type="submit" class="btn btn-default">Сохранить</button>
        </form>

        </div>

@endsection


@section('scripts')
    @parent

@endsection