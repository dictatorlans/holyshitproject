@extends('master')


@section('title')
Обучающие курсы
@endsection

@section('styles')
    @parent
    <link rel="stylesheet" href="assets/buddypress/buddypress-ver=0.2.css"/>
@endsection

@section('content')

<section id="coursestitle" style="">
    <div class="container">
        <div class="pagetitle">
        	<h1>All Courses</h1>
            <h5>Course directory</h5>        </div>
    </div>
</section>
<section id="content">
	<div id="buddypress">
    <div class="container">


		<div class="padder">

				<div class="row">
				<form action="" method="post" id="course-directory-form" class="dir-form">
					<div class="col-md-3 col-sm-4">
						<div class="item-list-tabs" role="navigation">
							<ul>
								<li class="selected" id="course-all"><a href="index.htm" >All Courses <span>11</span></a></li>

																							</ul>
						</div><!-- .item-list-tabs -->
						<ul class="breadcrumbs"><li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a rel="v:url" property="v:title" itemprop="url" href=""  ><span itemprop="name">Home</span></a></li><li class="current"><span itemprop="title">Software Training</span></li></ul>					<div id="course-dir-search" class="dir-search" role="search">
						<form action="" method="get" id="search-course-form">
		<label><input type="text" name="s" id="course_search" placeholder="Search ..." /></label>
		<input type="submit" id="course_search_submit" name="course_search_submit" value="Search" />
	</form>					</div><!-- #group-dir-search -->
					<div class="widget"><div class="course_filters"><h4>Course Categories</h4><ul class="category_filter"><li><input id="business" type="checkbox" class="bp-course-category-filter" name="bp-course-category-filter" value="business" /> <label for="business">Business</label></li><li><input id="technology" type="checkbox" class="bp-course-category-filter" name="bp-course-category-filter" value="technology" /> <label for="technology">Technology</label></li><li><input id="photography-2" type="checkbox" class="bp-course-category-filter" name="bp-course-category-filter" value="photography-2" /> <label for="photography-2">Photography</label></li></ul><h4>Type</h4><ul class="type_filter"><li><input id="all" type="radio" class="bp-course-free-filter" name="bp-course-free-filter" value="all" /> <label for="all">All</label></li><li><input id="free" type="radio" class="bp-course-free-filter" name="bp-course-free-filter" value="free" /> <label for="free">Free</label></li><li><input id="paid" type="radio" class="bp-course-free-filter" name="bp-course-free-filter" value="paid" /> <label for="paid">Paid</label></li></ul><a id="submit_filters" class="button full">Filter Results</a></div></div>	<div class="widget"><h4 class="widget_title">Advanced Course Search Widget</h4><form role="search" method="get" id="searchform" action="http://themes.vibethemes.com/wplms/skins/modern/">
		     			<input type="hidden" name="post_type" value="course" />
		     			<ul><li><select name="course-cat" class="chosen chzn-select"><option value="">Select Course Category</option><option value="business" >Business</option><option value="photography-2" >Photography</option><option value="technology" >Technology</option></select></li><li><select name="instructor" class="chosen chzn-select"><option value="">Select Instructor</option><option value="1" >Eddard Stark</option><option value="4" >Instructor asda</option></select></li><li><input type="text" value="" name="s" id="s" placeholder="Type Keywords.." /></li>
					     <li><input type="submit" id="searchsubmit" value="Search" /></li></ul>
					</form></div>	<div class="widget"><h4 class="widget_title">Popular Courses</h4><ul class="widget_course_list no-ajax">
	<li itemscope itemtype="http://schema.org/Product"><a href="../course/starting-a-startup/index.htm" ><img width="150" height="150" src="../wp-content/uploads/2015/09/course5-150x150.jpg"  class="attachment-thumbnail wp-post-image" alt="course5" /><h6><em itemprop="name">Starting a Startup</em><span><div class="modern-star-rating"><span class="dashicons dashicons-star-filled"></span><span class="dashicons dashicons-star-filled"></span><span class="dashicons dashicons-star-filled"></span><span class="dashicons dashicons-star-filled"></span><span class="dashicons dashicons-star-filled"></span></div>( <strong itemprop="reviewCount">1</strong> REVIEWS )<div class="students"><i class="icon-users"></i> 6 STUDENTS</div></span></h6></a></li><li itemscope itemtype="http://schema.org/Product"><a href="../course/marketing-management/index.htm" ><img width="150" height="150" src="../wp-content/uploads/2015/09/course4-150x150.jpg"  class="attachment-thumbnail wp-post-image" alt="course4" /><h6><em itemprop="name">Marketing Management</em><span><div class="modern-star-rating"><span class="dashicons dashicons-star-filled"></span><span class="dashicons dashicons-star-filled"></span><span class="dashicons dashicons-star-filled"></span><span class="dashicons dashicons-star-empty"></span><span class="dashicons dashicons-star-empty"></span></div>( <strong itemprop="reviewCount">1</strong> REVIEWS )<div class="students"><i class="icon-users"></i> 3 STUDENTS</div></span></h6></a></li><li itemscope itemtype="http://schema.org/Product"><a href="../course/basic-of-nature-photography/index.htm" ><img width="150" height="150" src="../wp-content/uploads/2015/09/course2-150x150.jpg"  class="attachment-thumbnail wp-post-image" alt="course2" /><h6><em itemprop="name">Basic of Nature Photography</em><span><div class="modern-star-rating"><span class="dashicons dashicons-star-filled"></span><span class="dashicons dashicons-star-filled"></span><span class="dashicons dashicons-star-filled"></span><span class="dashicons dashicons-star-filled"></span><span class="dashicons dashicons-star-empty"></span></div>( <strong itemprop="reviewCount">4</strong> REVIEWS )<div class="students"><i class="icon-users"></i> 3 STUDENTS</div></span></h6></a></li>	</ul>	</div>	<div class="widget"><h4 class="widget_title">Most Rated</h4><ul class="widget_course_list no-ajax">
	<li itemscope itemtype="http://schema.org/Product"><a href="../course/basic-of-nature-photography/index.htm" ><img width="150" height="150" src="../wp-content/uploads/2015/09/course2-150x150.jpg"  class="attachment-thumbnail wp-post-image" alt="course2" /><h6><em itemprop="name">Basic of Nature Photography</em><span><div class="modern-star-rating"><span class="dashicons dashicons-star-filled"></span><span class="dashicons dashicons-star-filled"></span><span class="dashicons dashicons-star-filled"></span><span class="dashicons dashicons-star-filled"></span><span class="dashicons dashicons-star-empty"></span></div>( <strong itemprop="reviewCount">4</strong> REVIEWS )<div class="students"><i class="icon-users"></i> 3 STUDENTS</div></span></h6></a></li><li itemscope itemtype="http://schema.org/Product"><a href="../course/software-training/index.htm" ><img width="150" height="150" src="../wp-content/uploads/2015/09/course9-150x150.jpg"  class="attachment-thumbnail wp-post-image" alt="course9" /><h6><em itemprop="name">Software Training</em><span><div class="modern-star-rating"><span class="dashicons dashicons-star-filled"></span><span class="dashicons dashicons-star-filled"></span><span class="dashicons dashicons-star-filled"></span><span class="dashicons dashicons-star-filled"></span><span class="dashicons dashicons-star-empty"></span></div>( <strong itemprop="reviewCount">3</strong> REVIEWS )<div class="students"><i class="icon-users"></i> 15 STUDENTS</div></span></h6></a></li><li itemscope itemtype="http://schema.org/Product"><a href="../course/how-to-write-effectively/index.htm" ><img width="150" height="150" src="../wp-content/uploads/2015/09/course1-150x150.jpg"  class="attachment-thumbnail wp-post-image" alt="course1" /><h6><em itemprop="name">How to write effectively</em><span><div class="modern-star-rating"><span class="dashicons dashicons-star-filled"></span><span class="dashicons dashicons-star-filled"></span><span class="dashicons dashicons-star-filled"></span><span class="dashicons dashicons-star-filled"></span><span class="dashicons dashicons-star-half"></span></div>( <strong itemprop="reviewCount">2</strong> REVIEWS )<div class="students"><i class="icon-users"></i> 1 STUDENTS</div></span></h6></a></li>	</ul>	</div>
				</div>
				<div class="col-md-9 col-sm-8">



					<div class="item-list-tabs" id="subnav" role="navigation">
						<ul>
														<li class="switch_view"><a id="list_view" class="active"><i class="icon-list-1"></i></a><a id="grid_view"><i class="icon-grid"></i></a>
							</li>
							<li id="course-order-select" class="last filter">

								<label for="course-order-by">Order By:</label>
								<select id="course-order-by">
																		<option value="">Select Order</option>
									<option value="newest">Newly Published</option>
									<option value="alphabetical">Alphabetical</option>
									<option value="popular">Most Members</option>
									<option value="rated">Highest Rated</option>

																	</select>
							</li>
						</ul>
					</div>
					<div id="course-dir-list" class="course dir-list">

							<div id="pag-top" class="pagination">

		<div class="pag-count" id="course-dir-count-top">

			Viewing page 1 of 2
		</div>

		<div class="pagination-links" id="course-dir-pag-top">

			<span class='page-numbers current'>1</span>
<a class='page-numbers' href="-items_page=2.htm" >2</a>
<a class="next page-numbers" href="-items_page=2.htm" >&rarr;</a>
		</div>

	</div>


	<ul id="course-list" class="item-list" role="main">



   		<li class="modern_course_single_item">
   			<div class="row">
				<div class="col-md-4">
					<div class="item-avatar" data-id="1260" itemprop="photo">
						<a href="../course/software-training/index.htm"  title="Software Training"><img width="768" height="460" src="../wp-content/uploads/2015/09/course9.jpg"  class="attachment-full wp-post-image" alt="course9" /></a>					</div>
				</div>

				<div class="col-md-6">
					<div class="item-title">
						<a href="../course/software-training/index.htm" >Software Training</a>					</div>
					<div class="item-desc"><ul class="course-category"><li><a href="../course-cat/technology/index.htm"  rel="tag">Technology</a></li></ul><p>Fusce in purus rutrum, tristique metus sed, laoreet mi. Nullam nec mauris in lorem blandit vehicula. Nulla felis lectus, luctus sed accumsan sit amet, posuere id elit. Mauris malesuada vulputate gravida</p>
<div class="instructors"><a href="../members/instructor/index.htm"  title="instructor"><img src="../wp-content/uploads/2015/09/asd.jpg"  class="avatar user-4-avatar avatar-150 photo" width="150" height="150" alt="Profile Photo" /></a></div></div>
				</div>
				<div class="col-md-2">
					<div class="course-meta">
						<ul>
								<li><span class="dashicons dashicons-groups"></span> 15</li>
								<li><span class="dashicons dashicons-clock"></span> 90 Days</li></ul><div class="modern-star-rating"><span class="dashicons dashicons-star-filled"></span><span class="dashicons dashicons-star-filled"></span><span class="dashicons dashicons-star-filled"></span><span class="dashicons dashicons-star-filled"></span><span class="dashicons dashicons-star-empty"></span></div>						<div class="item-credits">
						<strong itemprop="offers" itemscope itemtype="http://schema.org/Offer"><del><span class="amount" itemprop="price">&pound;29.00</span></del> <ins><span class="amount" itemprop="price">&pound;19.00</span></ins><span class="subs"> per  month</span></strong><i class="icon-wallet-money right"></i>						</div>
					</div>
				</div>
							</div>
		</li>




   		<li class="modern_course_single_item">
   			<div class="row">
				<div class="col-md-4">
					<div class="item-avatar" data-id="1250" itemprop="photo">
						<a href="../course/developing-mobile-apps/index.htm"  title="Developing Mobile Apps"><img width="768" height="460" src="../wp-content/uploads/2015/09/course7.jpg"  class="attachment-full wp-post-image" alt="course7" /></a>					</div>
				</div>

				<div class="col-md-6">
					<div class="item-title">
						<a href="../course/developing-mobile-apps/index.htm" >Developing Mobile Apps</a>					</div>
					<div class="item-desc"><ul class="course-category"><li><a href="../course-cat/technology/index.htm"  rel="tag">Technology</a></li></ul><p>Fusce in purus rutrum, tristique metus sed, laoreet mi. Nullam nec mauris in lorem blandit vehicula. Nulla felis lectus, luctus sed accumsan sit amet, posuere id elit. Mauris malesuada vulputate gravida</p>
<div class="instructors"><a href="../members/jorge/index.htm"  title="jorge"><img src="../wp-content/uploads/avatars/1/cc0248098d207038a1b62aecd6d3d9e1-bpthumb.jpg"  class="avatar user-1-avatar avatar-150 photo" width="150" height="150" alt="Profile Photo" /></a></div></div>
				</div>
				<div class="col-md-2">
					<div class="course-meta">
						<ul>
								<li><span class="dashicons dashicons-groups"></span> 3</li>
								<li><span class="dashicons dashicons-clock"></span> 80 Days</li></ul><div class="modern-star-rating"><span class="dashicons dashicons-star-filled"></span><span class="dashicons dashicons-star-filled"></span><span class="dashicons dashicons-star-filled"></span><span class="dashicons dashicons-star-empty"></span><span class="dashicons dashicons-star-empty"></span></div>						<div class="item-credits">
						<strong itemprop="offers" itemscope itemtype="http://schema.org/Offer"><del><span class="amount" itemprop="price">&pound;29.00</span></del> <ins><span class="amount" itemprop="price">&pound;19.00</span></ins><span class="subs"> per  week</span></strong><i class="icon-wallet-money right"></i>						</div>
					</div>
				</div>
							</div>
		</li>




   		<li class="modern_course_single_item">
   			<div class="row">
				<div class="col-md-4">
					<div class="item-avatar" data-id="1242" itemprop="photo">
						<a href="../course/how-to-write-effectively/index.htm"  title="How to write effectively"><img width="768" height="460" src="../wp-content/uploads/2015/09/course1.jpg"  class="attachment-full wp-post-image" alt="course1" /></a>					</div>
				</div>

				<div class="col-md-6">
					<div class="item-title">
						<a href="../course/how-to-write-effectively/index.htm" >How to write effectively</a>					</div>
					<div class="item-desc"><ul class="course-category"><li><a href="../course-cat/business/index.htm"  rel="tag">Business</a></li></ul><p>Fusce in purus rutrum, tristique metus sed, laoreet mi. Nullam nec mauris in lorem blandit vehicula. Nulla felis lectus, luctus sed accumsan sit amet, posuere id elit. Mauris malesuada vulputate gravida         </p>
<div class="instructors"><a href="../members/jorge/index.htm"  title="jorge"><img src="../wp-content/uploads/avatars/1/cc0248098d207038a1b62aecd6d3d9e1-bpthumb.jpg"  class="avatar user-1-avatar avatar-150 photo" width="150" height="150" alt="Profile Photo" /></a></div></div>
				</div>
				<div class="col-md-2">
					<div class="course-meta">
						<ul>
								<li><span class="dashicons dashicons-groups"></span> 1</li>
								<li><span class="dashicons dashicons-clock"></span> 90 Days</li></ul><div class="modern-star-rating"><span class="dashicons dashicons-star-filled"></span><span class="dashicons dashicons-star-filled"></span><span class="dashicons dashicons-star-filled"></span><span class="dashicons dashicons-star-filled"></span><span class="dashicons dashicons-star-half"></span></div>						<div class="item-credits">
						<strong itemprop="offers" itemscope itemtype="http://schema.org/Offer"><span class="amount" itemprop="price">&pound;20.00</span><span class="subs"> per  month</span></strong><i class="icon-wallet-money right"></i>						</div>
					</div>
				</div>
							</div>
		</li>




   		<li class="modern_course_single_item">
   			<div class="row">
				<div class="col-md-4">
					<div class="item-avatar" data-id="1235" itemprop="photo">
						<a href="../course/basic-of-nature-photography/index.htm"  title="Basic of Nature Photography"><img width="768" height="460" src="../wp-content/uploads/2015/09/course2.jpg"  class="attachment-full wp-post-image" alt="course2" /></a>					</div>
				</div>

				<div class="col-md-6">
					<div class="item-title">
						<a href="../course/basic-of-nature-photography/index.htm" >Basic of Nature Photography</a>					</div>
					<div class="item-desc"><ul class="course-category"><li><a href="../course-cat/photography-2/index.htm"  rel="tag">Photography</a></li></ul><p>                            Lorem ipsum dolor sit amet, consectetur adipiscing elit.                             </p>
<div class="instructors"><a href="../members/jorge/index.htm"  title="jorge"><img src="../wp-content/uploads/avatars/1/cc0248098d207038a1b62aecd6d3d9e1-bpthumb.jpg"  class="avatar user-1-avatar avatar-150 photo" width="150" height="150" alt="Profile Photo" /></a></div></div>
				</div>
				<div class="col-md-2">
					<div class="course-meta">
						<ul>
								<li><span class="dashicons dashicons-groups"></span> 3</li>
								<li><span class="dashicons dashicons-clock"></span> 60 Days</li></ul><div class="modern-star-rating"><span class="dashicons dashicons-star-filled"></span><span class="dashicons dashicons-star-filled"></span><span class="dashicons dashicons-star-filled"></span><span class="dashicons dashicons-star-filled"></span><span class="dashicons dashicons-star-empty"></span></div>						<div class="item-credits">
						<strong itemprop="offers" itemscope itemtype="http://schema.org/Offer"><del><span class="amount" itemprop="price">&pound;29.00</span></del> <ins><span class="amount" itemprop="price">&pound;19.00</span></ins><span class="subs"> per  week</span></strong><i class="icon-wallet-money right"></i>						</div>
					</div>
				</div>
							</div>
		</li>




   		<li class="modern_course_single_item">
   			<div class="row">
				<div class="col-md-4">
					<div class="item-avatar" data-id="1216" itemprop="photo">
						<a href="../course/starting-a-startup/index.htm"  title="Starting a Startup"><img width="768" height="460" src="../wp-content/uploads/2015/09/course5.jpg"  class="attachment-full wp-post-image" alt="course5" /></a>					</div>
				</div>

				<div class="col-md-6">
					<div class="item-title">
						<a href="../course/starting-a-startup/index.htm" >Starting a Startup</a>					</div>
					<div class="item-desc"><ul class="course-category"><li><a href="../course-cat/business/index.htm"  rel="tag">Business</a></li></ul><p>Fusce in purus rutrum, tristique metus sed, laoreet mi. Nullam nec mauris in lorem blandit vehicula. Nulla felis lectus, luctus sed accumsan sit amet, posuere id elit. Mauris malesuada vulputate gravida</p>
<div class="instructors"><a href="../members/jorge/index.htm"  title="jorge"><img src="../wp-content/uploads/avatars/1/cc0248098d207038a1b62aecd6d3d9e1-bpthumb.jpg"  class="avatar user-1-avatar avatar-150 photo" width="150" height="150" alt="Profile Photo" /></a></div></div>
				</div>
				<div class="col-md-2">
					<div class="course-meta">
						<ul>
								<li><span class="dashicons dashicons-groups"></span> 6 / 20</li>
								<li><span class="dashicons dashicons-clock"></span> 150 Days</li></ul><div class="modern-star-rating"><span class="dashicons dashicons-star-filled"></span><span class="dashicons dashicons-star-filled"></span><span class="dashicons dashicons-star-filled"></span><span class="dashicons dashicons-star-filled"></span><span class="dashicons dashicons-star-filled"></span></div>						<div class="item-credits">
						<strong itemprop="offers" itemscope itemtype="http://schema.org/Offer"><span class="amount" itemprop="price">&pound;19.00</span></strong><i class="icon-wallet-money right"></i>						</div>
					</div>
				</div>
							</div>
		</li>




   		<li class="modern_course_single_item">
   			<div class="row">
				<div class="col-md-4">
					<div class="item-avatar" data-id="1204" itemprop="photo">
						<a href="../course/social-media-management/index.htm"  title="Social Media Management"><img width="768" height="460" src="../wp-content/uploads/2015/09/course9.jpg"  class="attachment-full wp-post-image" alt="course9" /></a>					</div>
				</div>

				<div class="col-md-6">
					<div class="item-title">
						<a href="../course/social-media-management/index.htm" >Social Media Management</a>					</div>
					<div class="item-desc"><ul class="course-category"><li><a href="../course-cat/business/index.htm"  rel="tag">Business</a></li></ul><p>Fusce in purus rutrum, tristique metus sed, laoreet mi. Nullam nec mauris in lorem blandit vehicula. Nulla felis lectus, luctus sed accumsan sit amet, posuere id elit. Mauris malesuada vulputate gravida</p>
<div class="instructors"><a href="../members/jorge/index.htm"  title="jorge"><img src="../wp-content/uploads/avatars/1/cc0248098d207038a1b62aecd6d3d9e1-bpthumb.jpg"  class="avatar user-1-avatar avatar-150 photo" width="150" height="150" alt="Profile Photo" /></a></div></div>
				</div>
				<div class="col-md-2">
					<div class="course-meta">
						<ul>
								<li><span class="dashicons dashicons-groups"></span> 0 / 29</li>
								<li><span class="dashicons dashicons-clock"></span> 180 Days</li></ul><div class="modern-star-rating"><span class="dashicons dashicons-star-filled"></span><span class="dashicons dashicons-star-filled"></span><span class="dashicons dashicons-star-filled"></span><span class="dashicons dashicons-star-filled"></span><span class="dashicons dashicons-star-empty"></span></div>						<div class="item-credits">
						<strong itemprop="offers" itemscope itemtype="http://schema.org/Offer"><span class="amount" itemprop="price">&pound;49.00</span></strong><i class="icon-wallet-money right"></i>						</div>
					</div>
				</div>
							</div>
		</li>




   		<li class="modern_course_single_item">
   			<div class="row">
				<div class="col-md-4">
					<div class="item-avatar" data-id="1188" itemprop="photo">
						<a href="../course/how-to-become-a-powerful-speaker/index.htm"  title="How to become a Powerful Speaker"><img width="768" height="460" src="../wp-content/uploads/2015/09/course6.jpg"  class="attachment-full wp-post-image" alt="course6" /></a>					</div>
				</div>

				<div class="col-md-6">
					<div class="item-title">
						<a href="../course/how-to-become-a-powerful-speaker/index.htm" >How to become a Powerful Speaker</a>					</div>
					<div class="item-desc"><ul class="course-category"><li><a href="../course-cat/business/index.htm"  rel="tag">Business</a></li></ul><p>Fusce in purus rutrum, tristique metus sed, laoreet mi. Nullam nec mauris in lorem blandit vehicula. Nulla felis lectus, luctus sed accumsan sit amet, posuere id elit. Mauris malesuada vulputate gravida</p>
<div class="instructors"><a href="../members/jorge/index.htm"  title="jorge"><img src="../wp-content/uploads/avatars/1/cc0248098d207038a1b62aecd6d3d9e1-bpthumb.jpg"  class="avatar user-1-avatar avatar-150 photo" width="150" height="150" alt="Profile Photo" /></a></div></div>
				</div>
				<div class="col-md-2">
					<div class="course-meta">
						<ul>
								<li><span class="dashicons dashicons-groups"></span> 1</li>
								<li><span class="dashicons dashicons-clock"></span> 90 Days</li></ul><div class="modern-star-rating"><span class="dashicons dashicons-star-filled"></span><span class="dashicons dashicons-star-filled"></span><span class="dashicons dashicons-star-filled"></span><span class="dashicons dashicons-star-filled"></span><span class="dashicons dashicons-star-empty"></span></div>						<div class="item-credits">
						<strong itemprop="offers" itemscope itemtype="http://schema.org/Offer"><span class="amount" itemprop="price">&pound;19.00</span><span class="subs"> per  week</span></strong><i class="icon-wallet-money right"></i>						</div>
					</div>
				</div>
							</div>
		</li>




   		<li class="modern_course_single_item">
   			<div class="row">
				<div class="col-md-4">
					<div class="item-avatar" data-id="1176" itemprop="photo">
						<a href="../course/digital-photography-2/index.htm"  title="Digital Photography"><img width="768" height="460" src="../wp-content/uploads/2015/09/course3.jpg"  class="attachment-full wp-post-image" alt="course3" /></a>					</div>
				</div>

				<div class="col-md-6">
					<div class="item-title">
						<a href="../course/digital-photography-2/index.htm" >Digital Photography</a>					</div>
					<div class="item-desc"><ul class="course-category"><li><a href="../course-cat/photography-2/index.htm"  rel="tag">Photography</a></li></ul><p>Fusce in purus rutrum, tristique metus sed, laoreet mi. Nullam nec mauris in lorem blandit vehicula. Nulla felis lectus, luctus sed accumsan sit amet, posuere id elit. Mauris malesuada vulputate gravida</p>
<div class="instructors"><a href="../members/jorge/index.htm"  title="jorge"><img src="../wp-content/uploads/avatars/1/cc0248098d207038a1b62aecd6d3d9e1-bpthumb.jpg"  class="avatar user-1-avatar avatar-150 photo" width="150" height="150" alt="Profile Photo" /></a></div></div>
				</div>
				<div class="col-md-2">
					<div class="course-meta">
						<ul>
								<li><span class="dashicons dashicons-groups"></span> 1</li>
								<li><span class="dashicons dashicons-clock"></span> 60 Days</li></ul><div class="modern-star-rating"><span class="dashicons dashicons-star-empty"></span><span class="dashicons dashicons-star-empty"></span><span class="dashicons dashicons-star-empty"></span><span class="dashicons dashicons-star-empty"></span><span class="dashicons dashicons-star-empty"></span></div>						<div class="item-credits">
						<strong itemprop="offers" itemscope itemtype="http://schema.org/Offer"><span class="amount" itemprop="price">&pound;29.00</span><span class="subs"> per 2 weeks</span></strong><i class="icon-wallet-money right"></i>						</div>
					</div>
				</div>
							</div>
		</li>




   		<li class="modern_course_single_item">
   			<div class="row">
				<div class="col-md-4">
					<div class="item-avatar" data-id="1155" itemprop="photo">
						<a href="../course/how-to-develop-mobile-apps/index.htm"  title="How to develop Mobile Apps"><img width="768" height="460" src="../wp-content/uploads/2015/09/course8.jpg"  class="attachment-full wp-post-image" alt="course8" /></a>					</div>
				</div>

				<div class="col-md-6">
					<div class="item-title">
						<a href="../course/how-to-develop-mobile-apps/index.htm" >How to develop Mobile Apps</a>					</div>
					<div class="item-desc"><ul class="course-category"><li><a href="../course-cat/technology/index.htm"  rel="tag">Technology</a></li></ul><p>Fusce in purus rutrum, tristique metus sed, laoreet mi. Nullam nec mauris in lorem blandit vehicula. Nulla felis lectus, luctus sed accumsan sit amet, posuere id elit. Mauris malesuada vulputate gravida</p>
<div class="instructors"><a href="../members/jorge/index.htm"  title="jorge"><img src="../wp-content/uploads/avatars/1/cc0248098d207038a1b62aecd6d3d9e1-bpthumb.jpg"  class="avatar user-1-avatar avatar-150 photo" width="150" height="150" alt="Profile Photo" /></a></div></div>
				</div>
				<div class="col-md-2">
					<div class="course-meta">
						<ul>
								<li><span class="dashicons dashicons-groups"></span> 11</li>
								<li><span class="dashicons dashicons-clock"></span> 180 Days</li></ul><div class="modern-star-rating"><span class="dashicons dashicons-star-filled"></span><span class="dashicons dashicons-star-filled"></span><span class="dashicons dashicons-star-filled"></span><span class="dashicons dashicons-star-empty"></span><span class="dashicons dashicons-star-empty"></span></div>						<div class="item-credits">
						<strong itemprop="offers" itemscope itemtype="http://schema.org/Offer"><span class="amount" itemprop="price">&pound;29.00</span></strong><i class="icon-wallet-money right"></i>						</div>
					</div>
				</div>
							</div>
		</li>



	</ul>


	<div id="pag-bottom" class="pagination">

		<div class="pag-count" id="course-dir-count-bottom">

			Viewing page 1 of 2
		</div>

		<div class="pagination-links" id="course-dir-pag-bottom">

			<span class='page-numbers current'>1</span>
<a class='page-numbers' href="-items_page=2.htm" >2</a>
<a class="next page-numbers" href="-items_page=2.htm" >&rarr;</a>
		</div>

	</div>




					</div><!-- #courses-dir-list -->


					<input type="hidden" id="_wpnonce-course-filter" name="_wpnonce-course-filter" value="ef3061edae" /><input type="hidden" name="_wp_http_referer" value="/wplms/skins/modern/all-courses/" />

				</form>
			</div>
		</div>

		</div><!-- .padder -->

	</div><!-- #content -->
</div>
</section>


@endsection


@section('scripts')
    @parent

@endsection