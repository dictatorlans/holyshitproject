@extends('master')

@section('title')
Авторизация
@endsection

@section('styles')
    @parent

@endsection

@section('content')
    <form method="POST">
        <h2>Введите ваш логин и пароль</h2>
        Email <br><input type="email" name="email"><br>
        Пароль:<br> <input type="password" name="password"><br>
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="submit" value="Enter">
    </form>
@endsection


@section('scripts')
    @parent

@endsection