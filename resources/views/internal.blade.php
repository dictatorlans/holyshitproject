<!DOCTYPE html>
<html>
    <head>
        <script type='text/javascript' src="/assets/js/jquery-1.11.1.min.js"></script>
        <title>@yield('title')</title>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        @section('styles')
            <link rel="stylesheet" href="/assets/bootstrap/css/bootstrap.min.css"/>
            <link rel='stylesheet' id='style-css-css' href="/assets/css/style-main.css"/>
            <link rel='stylesheet' id='wplms-modern-css-css' href="/assets/wplms_modern/assets/css/wplms_modern.min.css-ver=4.3.1.css"/>
            <link rel='stylesheet' id='theme-css-css' href="/assets/wplms_modern/style.css-ver=4.3.1.css"/>
            <link rel='stylesheet' id='google-webfonts-css' href="https://fonts.googleapis.com/css?family=Lato:300,regular,700|Open+Sans:regular|Raleway:regular|Roboto:regular|Oswald:600.css"/>
            <link rel='stylesheet' id='evcal_google_fonts-css' href="https://fonts.googleapis.com/css?family=Oswald:400,300|Open+Sans:400,300&ver=4.3.1.css"/>
            <link rel='stylesheet' id='icons-css-css' href="/assets/vibe-shortcodes/css/fonticons.css-ver=4.3.1.css"/>
            <link rel='stylesheet' id='magnific-css-css' href="/assets/vibe-shortcodes/css/magnific-popup.css-ver=4.3.1.css"/>
            <link rel='stylesheet' id='animation-css-css' href="/assets/vibe-shortcodes/css/animation.css-ver=4.3.1.css"/>
            <link rel='stylesheet' id='shortcodes-css-css' href="/assets/vibe-shortcodes/css/shortcodes.css-ver=4.3.1.css"/>
            <link rel='stylesheet' id='dashicons-css' href="/assets/dashicons/dashicons.min.css-ver=4.3.1.css"/>
            <link rel='stylesheet' id='bp-course-css-css' href="/assets/vibe-course-module/includes/css/course_template.css-ver=4.3.1.css"/>
            <link rel='stylesheet' id='search-css-css' href="/assets/css/chosen.css-ver=4.3.1.css"/>
            <link rel='stylesheet' id='select2-css' href="/assets/css/select2.css-ver=4.3.1.css"/>

        @show
    </head>
    <body class="directory course buddypress page page-parent page-template-default">
        <main>
            <div id="global" class="global">
                @include('layouts.sidebar')
                <div class="pusher">
                    @include('layouts.headertop')
                    @include('layouts.header')
                    <section id="coursestitle" style="background:url(/assets/images/default.jpeg)">
                        <div class="container">
                            <div class="pagetitle">
                            	<h1>@yield('CategoryTitle')</h1>
                                <h5>@yield('title')</h5>
                             </div>
                        </div>
                    </section>
                    <section id="content">
                    @section('content')
                        Content here
                    @show
                    </section>
                    @include('layouts.footer')
                </div>
            </div>
        </main>
        @section('scripts')
            <script type='text/javascript' src="/assets/js/modernizr-2.8.3.min.js"></script>
            <script type='text/javascript' src="/assets/js/jquery.magnific-popup.min.js-ver=1.0.js"></script>
            <script type='text/javascript' src="/assets/wplms_modern/assets/js/wplms_modern.min.js-ver=4.3.1.js"></script>
            <script type='text/javascript' src="/assets/js/nprogress.js-ver=4.3.1.js"></script>
            <script type='text/javascript' src="/assets/js/sidebarEffects.js-ver=4.3.1.js"></script>
            <script type='text/javascript' src="/assets/js/jquery.flexslider-min.js-ver=1.0.js"></script>
            <script type='text/javascript' src="/assets/js/custom.js-ver=4.3.1.js"></script>
            <script>NProgress.start();</script>
        @show
    </body>
</html>
